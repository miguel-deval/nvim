call plug#begin()
Plug 'lervag/vimtex'
Plug 'maverickg/stan.vim'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'python-mode/python-mode', { 'branch': 'develop' }
Plug 'vimwiki/vimwiki', { 'branch': 'dev' }
Plug 'morhetz/gruvbox'
Plug 'blindFS/vim-taskwarrior'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'junegunn/vim-easy-align'
Plug 'vim-syntastic/syntastic'
" Plug 'w0rp/ale'
" Plug 'tbabej/taskwiki'
call plug#end()

" Use Vim's system runtimepath
set rtp+=/usr/share/vim/vimfiles

" this is needed for vimwiki
set nocompatible
filetype plugin indent on
set background=dark
syntax enable
colorscheme gruvbox

set modeline
set ruler

source ~/.vim/astro-ph.vim

" vimtex settings
let g:tex_flavor = "latex"
let g:vimtex_view_general_viewer = "mupdf"
let g:vimtex_fold_enabled = 1

set wildmode=longest,list:longest,list:full

" python mode
" let g:pymode_trim_whitespaces = 0

" CtrlP
let g:ctrlp_cmd = 'CtrlPBuffer'

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" let g:syntastic_c_checkers=['gcc','splint']
" let g:syntastic_mode_map = { 'passive_filetypes': ['python'] }
