
function! Astroph(number)
   execute "silent !getarxiv.sh -dsrc " . a:number
   redraw!
   if strlen(a:number) <= 4
      let arxivnr=system("echo -n `date +%y%m`") . "." . printf("%04u",a:number)
      echo arxivnr
   else
      let arxivnr=a:number
   endif
   let filename="~/preprints/astro-ph/".arxivnr."/".arxivnr.".tex"
   execute "edit " . filename
endfunction

function! ADS(number)
   execute 'silent !getads.sh -dtxt "' . a:number . '"'
   redraw!
   let filename="~/preprints/ADS/" . a:number . ".txt"
   execute "edit " . filename
endfunction

